from flask import Flask
from flask_restful import Resource, Api
import RPistepper as stp


app = Flask(__name__)

M1_pins = [17, 27, 10, 9]

@app.route('/test/<stpes>')
def hello_world(stpes):
    print(stpes)
    rotate_steps = int(stpes)
    with stp.Motor(M1_pins) as M1:
        M1.move(rotate_steps)
        M1.release()
    return {'message':rotate_steps}           

      
